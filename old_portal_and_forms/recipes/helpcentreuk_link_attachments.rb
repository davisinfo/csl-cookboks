node[:deploy].each do |app_name, deploy|
  if app_name == 'helpcentreuk'
    link "#{deploy[:deploy_to]}/current/attachments" do
      to "/mnt/helpcentreuk-attachments"
    end
  end
end